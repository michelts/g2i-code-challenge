Trivia Game Coding Challenge
============================

This is the implementation for the Trivia Game for the G2i coding challenge.

Notes to reviewers
------------------

I have some notes about the project for the reviewers:

* The project was built using expo so, to test it, you just need to
  run `expo start`;
* I'm not using Typescript daily, but I used `prop-type` for type checking,
  although I'm learning typescript right now;
* I decided to allow the user to navigate back through the steps, so the user
  would be able to change his answers until play again. A future release
  probably would need to prevent the user change answers after the
  last one been answered.
* The tests can be run through `yarn run tests`;
* The linter (eslint) can be run through `yarn run lint`;
* You received the `.git/` folder as well. You can follow my line of thinking
  or see I'm using TDD correctly by inspecting the commits.

Specifications
""""""""""""""

I focused strictly in the project specifications, I mean, I didn't provide a
way to pick the difficulty level or number of questions, although this is a
interesting future upgrade.

In real projects, I use to focus on the most important functionalities in the
client point of view, so I can deliver an MVP sooner.

I reduced the refactorings needed by changes in the client's mind by following
this approach.

Layout
""""""

I also draw some simple layout screens using Inkscape, just to give me some
directions before actually start coding.

These layout screens are in the layouts folder.
