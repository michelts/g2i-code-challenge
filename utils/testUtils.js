import React from 'react';
import { PropTypes } from 'prop-types';
import { render as rtlRender } from '@testing-library/react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from '../reducers';

export const dispatch = jest.fn();

export function renderWithStore(
  ui,
  {
    initialState,
    store = createStore(rootReducer, initialState),
    ...renderOptions
  } = {},
) {
  function Wrapper({ children }) {
    const wrappedStore = { ...store, dispatch };
    return <Provider store={wrappedStore}>{children}</Provider>;
  }

  Wrapper.propTypes = {
    children: PropTypes.node.isRequired,
  };

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}
