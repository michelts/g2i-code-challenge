import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import entitiesDecode from 'html-entities-decoder';
import Container from '../components/Container';
import Stepper from '../components/Stepper';
import Button from '../components/Button';
import styles from '../styles';

const Quiz = ({ navigation, route: { params: { question: questionIndex } } }) => {
  const question = useSelector((state) => state.questions.get(questionIndex), shallowEqual);
  const questionsCount = useSelector((state) => state.questions.size);
  const dispatch = useDispatch();

  const handlePress = React.useCallback((value) => {
    dispatch({
      type: 'QUESTION_ANSWERED',
      payload: { question: questionIndex, answer: value },
    });

    const nextIndex = questionIndex + 1;
    if (nextIndex < questionsCount) { // zero based indexing
      navigation.push('Quiz', { question: nextIndex });
    } else {
      navigation.navigate('Results');
    }
  }, [questionsCount, questionIndex]);

  return (
    <Container>
      <View style={styles.category}>
        <Text style={styles.text2}>
          {question.category}
        </Text>
      </View>
      <View style={styles.card}>
        <Stepper
          step={questionIndex + 1}
          stepsCount={questionsCount}
        />
        <Text style={[styles.text1, styles.inverse]}>
          {entitiesDecode(question.question)}
        </Text>
      </View>
      <View style={styles.buttonGroup}>
        <Button onPress={() => handlePress(true)}>
          Yes
        </Button>
        <Button onPress={() => handlePress(false)}>
          No
        </Button>
      </View>
    </Container>
  );
};

Quiz.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
  }).isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({
      question: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
};
export default Quiz;
