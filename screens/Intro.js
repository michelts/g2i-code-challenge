import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import settings from '../settings';
import Container from '../components/Container';
import Loading from '../components/Loading';
import ErrorIndicator from '../components/ErrorIndicator';
import Button from '../components/Button';
import styles from '../styles';

const Intro = ({ navigation }) => {
  const [isLoading, setLoading] = React.useState(false);
  const [hasError, setHasError] = React.useState(false);
  const dispatch = useDispatch();

  const handleBegin = React.useCallback(() => {
    setLoading(true);
    setHasError(false);
    axios.get(settings.questionsSource)
      .then(({ data: { response_code: code, results } }) => {
        if (code === 0) {
          dispatch({ type: 'QUESTIONS_LOADED', payload: results });
          navigation.navigate('Quiz', { question: 0 });
        } else {
          setHasError(true);
        }
        setLoading(false);
      })
      .catch(() => {
        setHasError(true);
        setLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <Container>
        <Loading />
      </Container>
    );
  }

  if (hasError) {
    return (
      <Container>
        <ErrorIndicator onPress={handleBegin}>
          Error while loading questions, are you offline?
          {' '}
          You need to be online to be able to load them.
        </ErrorIndicator>
      </Container>
    );
  }

  return (
    <Container>
      <View style={styles.centered}>
        <Text style={styles.text1}>Welcome to the</Text>
        <Text style={styles.title1}>Trivia Challenge!</Text>
      </View>
      <Text style={styles.text1}>
        You will be presented with 10 True or False questions.
      </Text>
      <Text style={styles.text1}>
        Can you score 100%?
      </Text>
      <Button onPress={handleBegin}>
        Begin
      </Button>
    </Container>
  );
};

Intro.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Intro;
