import React from 'react';
import { fireEvent, waitForElementToBeRemoved } from '@testing-library/react-native';
import axios from 'axios';
import { renderWithStore as render, dispatch } from '../../utils/testUtils';
import { QuestionsDataFactory, QuestionsDataFailFactory } from '../../factories';
import Intro from '../Intro';

describe('Intro component', () => {
  let navigation;
  let initialState;
  let questionsData;

  beforeEach(() => {
    navigation = { navigate: jest.fn() };
    initialState = { questions: null };

    questionsData = QuestionsDataFactory.build();
    axios.get.mockReset();
    axios.get.mockResolvedValue({ data: questionsData });
  });

  it('should render instructions and a begin button', () => {
    const { getByText } = render(<Intro navigation={navigation} />, { initialState });
    getByText('Welcome to the');
    getByText('Trivia Challenge!');
    getByText('You will be presented with 10 True or False questions.');
    getByText('Can you score 100%?');
    getByText('Begin');
  });

  it('should load questions, show a loading indicator and navigate to 1st question when Begin is clicked', async () => {
    const { getByText } = render(<Intro navigation={navigation} />);
    const beginButton = getByText('Begin');
    fireEvent.press(beginButton);

    // Begin button would cause a loading indicator to be shown until questions are there
    getByText('Loading...');
    expect(axios.get).toHaveBeenCalledWith('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean');

    // After axios processing and dispatching, Loading must disappear and Intro screen shown again
    await waitForElementToBeRemoved(() => getByText('Loading...'));
    expect(dispatch).toHaveBeenCalledWith({ type: 'QUESTIONS_LOADED', payload: questionsData.results });
    expect(navigation.navigate).toHaveBeenCalledWith('Quiz', { question: 0 });
    getByText('Begin');
  });

  describe('should show an error indicator and allow retry when error', () => {
    const testErrorAndRetry = async () => {
      const { getByText } = render(<Intro navigation={navigation} />);
      const beginButton = getByText('Begin');
      fireEvent.press(beginButton);

      // Begin button would cause a loading indicator to be shown until error is processed
      getByText('Loading...');
      expect(axios.get).toHaveBeenCalledWith('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean');

      // After axios processing, loading must be replaced by the error message
      await waitForElementToBeRemoved(() => getByText('Loading...'));
      getByText('Error while loading questions, are you offline? You need to be online to be able to load them.');

      // A click in the retry button would repeat the process
      const retryButton = getByText('Retry');
      fireEvent.press(retryButton);

      getByText('Loading...');
      expect(axios.get).toHaveBeenCalledTimes(2);

      // After axios success and actions dispatch, loading must be replaced by the intro screen
      await waitForElementToBeRemoved(() => getByText('Loading...'));
      expect(dispatch).toHaveBeenCalledWith({ type: 'QUESTIONS_LOADED', payload: questionsData.results });
      expect(navigation.navigate).toHaveBeenCalledWith('Quiz', { question: 0 });
      getByText('Begin');
    };

    it('if questions response_code is different from zero', async () => {
      axios.get
        .mockResolvedValueOnce({ data: QuestionsDataFailFactory.build() })
        .mockResolvedValueOnce({ data: questionsData });
      await testErrorAndRetry();
    });

    it('if questions response is not successful', async () => {
      axios.get
        .mockRejectedValueOnce({})
        .mockResolvedValueOnce({ data: questionsData });
      await testErrorAndRetry();
    });
  });
});
