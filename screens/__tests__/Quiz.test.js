import React from 'react';
import { fireEvent } from '@testing-library/react-native';
import entitiesDecode from 'html-entities-decoder';
import { renderWithStore as render, dispatch } from '../../utils/testUtils';
import Quiz from '../Quiz';
import { StoreFactory } from '../../factories';

jest.mock('html-entities-decoder', () => jest.fn((val) => val));

describe('Quiz component', () => {
  let navigation;
  let initialState;

  beforeEach(() => {
    navigation = {
      navigate: jest.fn(),
      push: jest.fn(),
    };
    initialState = StoreFactory.build();
  });

  it("should render the store's current answer category, question and action buttons", () => {
    const route = { params: { question: 0 } };
    const { getByText } = render(<Quiz navigation={navigation} route={route} />, { initialState });

    const question = initialState.questions.get(0);
    getByText(question.category);
    getByText(`1 of ${initialState.questions.size}`);
    getByText(question.question);
    getByText('Yes');
    getByText('No');
  });

  it('should unescape html entities from the question text', () => {
    const route = { params: { question: 0 } };
    render(<Quiz navigation={navigation} route={route} />, { initialState });

    const question = initialState.questions.get(0);
    expect(entitiesDecode).toHaveBeenCalledWith(question.question);
  });

  describe('Yes/No buttons', () => {
    const buttonChoices = [['Yes', true], ['No', false]];

    buttonChoices.forEach(([button, value]) => {
      it(`should save question answer as ${value} and move to next when click ${button} button`, () => {
        const questionId = 3;
        const route = { params: { question: questionId } };
        const { getByText } = render(
          <Quiz navigation={navigation} route={route} />, { initialState },
        );

        const buttonElem = getByText(button);
        fireEvent.press(buttonElem);

        expect(dispatch).toHaveBeenCalledWith({
          type: 'QUESTION_ANSWERED',
          payload: { question: questionId, answer: value },
        });
        expect(navigation.push).toHaveBeenCalledWith('Quiz', { question: questionId + 1 });
      });

      it(`should save question answer as ${value} and move to results when click ${button} button at last question`, () => {
        const questionId = initialState.questions.size - 1; // zero based indexing
        const route = { params: { question: questionId } };
        const { getByText } = render(
          <Quiz navigation={navigation} route={route} />, { initialState },
        );

        const buttonElem = getByText(button);
        fireEvent.press(buttonElem);

        expect(dispatch).toHaveBeenCalledWith({
          type: 'QUESTION_ANSWERED',
          payload: { question: questionId, answer: value },
        });
        expect(navigation.navigate).toHaveBeenCalledWith('Results');
      });
    });
  });
});
