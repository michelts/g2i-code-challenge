import React from 'react';
import { fireEvent, within } from '@testing-library/react-native';
import { renderWithStore as render } from '../../utils/testUtils';
import Results from '../Results';
import { AnsweredStoreFactory } from '../../factories';

describe('Results component', () => {
  let navigation;
  let initialState;
  const questionsCount = 3;
  const correctAnswersCount = 1;

  beforeEach(() => {
    navigation = { navigate: jest.fn() };
    initialState = AnsweredStoreFactory.build({}, { questionsCount, correctAnswersCount });
  });

  it('should render the answers score, each correct/incorrect question and a button to play again', () => {
    const {
      getByText, getAllByTestId,
    } = render(<Results navigation={navigation} />, { initialState });

    getByText('You scored');
    getByText(new RegExp(`${correctAnswersCount}\\s+of\\s+${questionsCount}`));

    const rows = getAllByTestId('result-item');
    expect(rows.length).toEqual(questionsCount);

    initialState.questions.forEach((question, index) => {
      const row = rows[index];
      const { getByTestId } = within(row);
      getByText(question.question);

      const image = getByTestId('result-item-image');
      expect(image.props.source.testUri).toEqual(
        `../../../assets/images/${!question.isCorrect ? 'in' : ''}correct.png`,
      );
    });

    getByText('Play again?');
  });

  it('should navigate to Intro when press the play again button', () => {
    const { getByText } = render(<Results navigation={navigation} />, { initialState });

    const buttonElem = getByText('Play again?');
    fireEvent.press(buttonElem);

    expect(navigation.navigate).toHaveBeenCalledWith('Intro');
  });
});
