import React from 'react';
import PropTypes from 'prop-types';
import { Text, ScrollView } from 'react-native';
import { useSelector, shallowEqual } from 'react-redux';
import { createSelector } from 'reselect';
import Container from '../components/Container';
import Button from '../components/Button';
import ResultItem from '../components/ResultItem';
import styles from '../styles';

const selectCorrectAnswersCount = createSelector(
  (state) => state.questions,
  (questions) => questions.filter((question) => question.isCorrect).size,
);

const Results = ({ navigation }) => {
  const questions = useSelector((state) => state.questions, shallowEqual);
  const correctAnswersCount = useSelector(selectCorrectAnswersCount);

  const handleRestart = React.useCallback(() => {
    navigation.navigate('Intro');
  });

  return (
    <Container>
      <Text style={styles.text3}>You scored</Text>
      <Text style={styles.title1}>
        {correctAnswersCount}
        {' '}
        of
        {' '}
        {questions.size}
      </Text>
      <ScrollView style={styles.card}>
        <Text style={[styles.text3, styles.inverse]}>
          Check out your answers:
        </Text>
        {questions.map((question) => (
          <ResultItem
            key={question.question}
            isCorrect={question.isCorrect}
          >
            {question.question}
          </ResultItem>
        ))}
      </ScrollView>
      <Button onPress={handleRestart}>
        Play again?
      </Button>
    </Container>
  );
};

Results.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Results;
