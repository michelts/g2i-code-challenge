import React from 'react';
import PropTypes from 'prop-types';
import { ImageBackground } from 'react-native';
import styles from '../styles';

const Container = ({ children }) => (
  <ImageBackground
    source={require('../assets/images/background.png')}
    style={styles.container}
  >
    {children}
  </ImageBackground>
);

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Container;
