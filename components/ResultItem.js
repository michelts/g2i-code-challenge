import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, Image } from 'react-native';
import entitiesDecode from 'html-entities-decoder';
import styles from '../styles';

const ResultItem = ({ children, isCorrect }) => (
  <View
    style={styles.answer}
    testID="result-item"
  >
    <View style={styles.answerIconWrapper}>
      <Image
        style={styles.answerIcon}
        source={
          isCorrect
            ? require('../assets/images/correct.png')
            : require('../assets/images/incorrect.png')
        }
        testID="result-item-image"
      />
    </View>
    <Text style={[styles.text3, styles.inverse]}>
      {entitiesDecode(children)}
    </Text>
  </View>
);

ResultItem.propTypes = {
  children: PropTypes.node.isRequired,
  isCorrect: PropTypes.bool.isRequired,
};

export default ResultItem;
