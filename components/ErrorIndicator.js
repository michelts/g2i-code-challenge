import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import Button from './Button';
import styles from '../styles';

const ErrorIndicator = ({ onPress: handlePress, children }) => (
  <View style={styles.centered}>
    <Text style={styles.title1}>Error!</Text>
    <Text style={[styles.text3, styles.centered, styles.verticallySpaced]}>
      {children}
    </Text>
    <Button onPress={handlePress}>
      Retry
    </Button>
  </View>
);

ErrorIndicator.propTypes = {
  onPress: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default ErrorIndicator;
