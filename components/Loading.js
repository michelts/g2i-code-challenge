import React from 'react';
import { Text, View } from 'react-native';
import styles from '../styles';

const Loading = () => (
  <View style={styles.loading}>
    <Text style={[styles.loadingText]}>
      Loading...
    </Text>
  </View>
);

export default Loading;
