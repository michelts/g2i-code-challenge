import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import styles from '../styles';

const Stepper = ({ step: userStep, stepsCount }) => {
  const steps = Array.from({ length: stepsCount }).map((_, index) => index);
  return (
    <>
      <View style={styles.progressBar}>
        {steps.map((step) => (
          <View
            key={step}
            style={[
              styles.progressBarItem,
              step === 0 ? styles.progressBarItemFirst : {},
              step < userStep ? styles.progressBarItemActive : {},
            ]}
          />
        ))}
      </View>
      <Text style={[styles.text3, styles.inverse]}>
        {userStep}
        {' '}
        of
        {' '}
        {stepsCount}
      </Text>
    </>
  );
};

Stepper.propTypes = {
  step: PropTypes.number.isRequired,
  stepsCount: PropTypes.number.isRequired,
};

export default Stepper;
