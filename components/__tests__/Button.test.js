import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../Button';

describe('Button', () => {
  it('should render correctly', () => {
    const onPress = jest.fn().mockName('onPress');
    const component = renderer.create(<Button onPress={onPress}>Child</Button>);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
