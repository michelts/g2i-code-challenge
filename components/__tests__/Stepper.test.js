import React from 'react';
import renderer from 'react-test-renderer';
import Stepper from '../Stepper';

describe('Stepper', () => {
  it('should render a text `step of stepsCount` with a progress bar with a view for each step active of not active', () => {
    const step = 1;
    const stepsCount = 2;
    const component = renderer.create(<Stepper step={step} stepsCount={stepsCount} />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
