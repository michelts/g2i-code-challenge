import React from 'react';
import renderer from 'react-test-renderer';
import ErrorIndicator from '../ErrorIndicator';

describe('ErrorIndicator', () => {
  it('should render the given error message and link the onPress to the retry button', () => {
    const onPress = jest.fn().mockName('onPress');
    const component = renderer.create(<ErrorIndicator onPress={onPress}>Child</ErrorIndicator>);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
