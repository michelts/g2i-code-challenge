import React from 'react';
import renderer from 'react-test-renderer';
import entitiesDecode from 'html-entities-decoder';
import ResultItem from '../ResultItem';

jest.mock('html-entities-decoder', () => jest.fn((val) => val));

describe('ResultItem', () => {
  it('should render children content with an icon indicating it is correct', () => {
    const component = renderer.create(<ResultItem isCorrect>Child</ResultItem>);
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should render children content with an icon indicating it is incorrect', () => {
    const component = renderer.create(<ResultItem isCorrect={false}>Child</ResultItem>);
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should unescape html entities from children', () => {
    const content = 'Text content';
    renderer.create(<ResultItem isCorrect>{content}</ResultItem>);
    expect(entitiesDecode).toHaveBeenCalledWith(content);
  });
});
