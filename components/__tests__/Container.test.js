import React from 'react';
import renderer from 'react-test-renderer';
import Container from '../Container';

describe('Container', () => {
  it('should render correctly', () => {
    const component = renderer.create(<Container>Child</Container>);
    expect(component.toJSON()).toMatchSnapshot();
  });
});
