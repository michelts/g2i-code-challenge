import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity } from 'react-native';
import styles from '../styles';

const Button = ({ onPress: handlePress, children }) => (
  <TouchableOpacity onPress={handlePress}>
    <View style={styles.button}>
      <Text style={styles.buttonLabel}>
        {children}
      </Text>
    </View>
  </TouchableOpacity>
);

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Button;
