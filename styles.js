import { StyleSheet } from 'react-native';

const baseStyles = {
  title: {
    fontFamily: 'FredokaOne',
    color: '#ffffff',
    textShadowColor: 'rgba(255, 255, 255, 0.5)',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 4,
  },
  text: {
    fontFamily: 'Chilanka',
    color: '#ffffff',
    alignItems: 'center',
    textAlign: 'center',
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#330b58',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 20,
  },
  text1: {
    ...baseStyles.text,
    fontSize: 24,
  },
  text2: {
    ...baseStyles.text,
    fontSize: 18,
  },
  text3: {
    ...baseStyles.text,
    fontSize: 16,
  },
  inverse: {
    color: '#1a1a1a',
    textAlign: 'left',
    marginBottom: 5,
  },
  title1: {
    ...baseStyles.title,
    fontSize: 32,
  },
  buttonGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  button: {
    paddingHorizontal: 18,
    paddingVertical: 6,
    borderRadius: 6,
    backgroundColor: '#7d24c9',
    shadowColor: '#7d24c9',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 4,
  },
  buttonLabel: {
    fontFamily: 'FredokaOne',
    fontSize: 36,
    color: '#ffffff',
    textTransform: 'uppercase',
    textShadowColor: 'rgba(255, 255, 255, 0.5)',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 2,
  },
  category: {
    marginTop: 20,
  },
  card: {
    backgroundColor: '#ffffff',
    padding: 20,
    marginVertical: 20,
    flexGrow: 1,
  },
  answer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingRight: 50,
  },
  answerIconWrapper: {
    paddingRight: 10,
  },
  answerIcon: {
    width: 20,
    height: 20,
  },
  loadingText: {
    fontFamily: 'Chilanka',
    fontSize: 18,
    color: '#ffffff',
  },
  verticallySpaced: {
    marginVertical: 20,
  },
  progressBar: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginBottom: 4,
  },
  progressBarItem: {
    flexGrow: 1,
    height: 10,
    backgroundColor: '#888888',
    color: 'black',
    marginLeft: 2,
  },
  progressBarItemFirst: {
    marginLeft: 0,
  },
  progressBarItemActive: {
    backgroundColor: '#832acf',
  },
});

export default styles;
