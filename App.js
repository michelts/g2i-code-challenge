import React from 'react';
import { AppLoading } from 'expo';
import { useFonts } from '@use-expo/font';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import Intro from './screens/Intro';
import Quiz from './screens/Quiz';
import Results from './screens/Results';

const Stack = createStackNavigator();

const store = createStore(rootReducer);

export default function App() {
  const [fontsLoaded] = useFonts({
    Chilanka: require('./assets/fonts/chilanka.ttf'),
    FredokaOne: require('./assets/fonts/fredoka-one.ttf'),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Intro">
          <Stack.Screen
            name="Intro"
            component={Intro}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Quiz"
            component={Quiz}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Results"
            component={Results}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
