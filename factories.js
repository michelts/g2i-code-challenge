import { Factory } from 'rosie';
import { List } from 'immutable';

export const QuestionDataFactory = new Factory()
  .sequence('category', (index) => `Category ${index}`)
  .sequence('question', (index) => `Question ${index}`)
  .sequence('correct_answer', (index) => ((index % 2) ? 'True' : 'False'))
  .attr('incorrect_answers', ['correct_answer'], (answer) => ((answer === 'True') ? 'False' : 'True'));

export const QuestionsDataFactory = new Factory()
  .attr('response_code', 0)
  .option('resultsCount', 10)
  .attr('results', ['resultsCount'], (count) => QuestionDataFactory.buildList(count));

export const QuestionsDataFailFactory = new Factory()
  .attr('response_code', 2)
  .attr('results', []);

export const QuestionItemFactory = new Factory()
  .sequence('category', (index) => `Category ${index}`)
  .sequence('question', (index) => `Question ${index}`)
  .sequence('correctAnswer', (index) => ((index % 2 === 0)))
  .attr('incorrectAnswers', ['correctAnswer'], (answer) => (!answer))
  .attr('userAnswer', null)
  .attr('isCorrect', ['correctAnswer', 'userAnswer'], (correct, user) => {
    if (user === null) {
      return undefined;
    }
    return (user === correct);
  });

export const StoreFactory = new Factory()
  .option('questionsCount', 10)
  .attr('questions', ['questionsCount'], (count) => List(QuestionItemFactory.buildList(count)));

export const AnsweredStoreFactory = new Factory()
  .option('questionsCount', 10)
  .option('correctAnswersCount', 3)
  .attr(
    'questions',
    ['questionsCount', 'correctAnswersCount'],
    (total, correct) => (
      List(QuestionItemFactory.buildList(
        correct, { correctAnswer: true, userAnswer: true },
      ))
        .concat(QuestionItemFactory.buildList(
          total - correct, { correctAnswer: true, userAnswer: false },
        ))
    ),
  );
