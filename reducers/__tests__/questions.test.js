import reducer from '../index';
import { QuestionDataFactory } from '../../factories';

describe('questions reducer', () => {
  it('should use null as default state', () => {
    const state = reducer(undefined, { type: undefined });
    expect(state.questions).toBe(null);
  });

  it('should store a List of questions when QUESTIONS_LOADED', () => {
    const questions = QuestionDataFactory.buildList(3);
    const state = reducer(undefined, { type: 'QUESTIONS_LOADED', payload: questions });
    expect(state.questions).toMatchSnapshot();
  });

  it('should set the question user answer and a flag to tell if it is correct at QUESTION_ANSWERED', () => {
    const questions = QuestionDataFactory.buildList(3, { correct_answer: 'True' });
    const intermediateState = reducer(undefined, { type: 'QUESTIONS_LOADED', payload: questions });
    const { questions: state } = reducer(intermediateState, { type: 'QUESTION_ANSWERED', payload: { question: 1, answer: true } });
    expect(state.get(0).userAnswer).toBe(null);
    expect(state.get(0).isCorrect).toBe(undefined);

    expect(state.get(1).userAnswer).toBe(true);
    expect(state.get(1).isCorrect).toBe(true);

    expect(state.get(2).userAnswer).toBe(null);
    expect(state.get(2).isCorrect).toBe(undefined);
  });

  it('should set the question as incorrect if the answer is incorrect at QUESTION_ANSWERED', () => {
    const questions = QuestionDataFactory.buildList(1, { correct_answer: 'False' });
    const intermediateState = reducer(undefined, { type: 'QUESTIONS_LOADED', payload: questions });
    const { questions: state } = reducer(intermediateState, { type: 'QUESTION_ANSWERED', payload: { question: 0, answer: true } });
    expect(state.get(0).userAnswer).toBe(true);
    expect(state.get(0).isCorrect).toBe(false);
  });
});
