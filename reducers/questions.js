import { List } from 'immutable';

const apiToObj = (obj) => ({
  category: obj.category,
  question: obj.question,
  correctAnswer: obj.correct_answer === 'True',
  userAnswer: null,
  isCorrect: undefined,
});

export default function questions(state = null, action) {
  switch (action.type) {
    case 'QUESTIONS_LOADED':
      return List(action.payload.map(apiToObj));

    case 'QUESTION_ANSWERED':
      return state.update(
        action.payload.question,
        (obj) => ({
          ...obj,
          userAnswer: action.payload.answer,
          isCorrect: action.payload.answer === obj.correctAnswer,
        }),
      );

    default:
      return state;
  }
}
